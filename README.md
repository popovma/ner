

# Пример

```python3
import ada
from ner.NER import process_input_
```
----
## В качестве входных данных используется текстовая строка
```python3
text ='''Глава Чечни Рамзан Кадыров прокомментировал информацию о том, что в Сирию был направлен сводный отряд, состоящий из бойцов батальонов специального назначения «Восток» и «Запад» Минобороны России.'''
print(text)
Глава Чечни Рамзан Кадыров прокомментировал информацию о том, что в Сирию был направлен сводный отряд, состоящий из бойцов батальонов специального назначения «Восток» и «Запад» Минобороны России.
```
---
## Извлечение именнованных сущностей
index - индекс начального символа

len - длина сущности

text -  именованная сущность

type - тип сущности

```python3

process_input_(text)
[{'index': 6, 'len': 5, 'text': 'Чечни', 'type': 'LOC'},
 {'index': 12, 'len': 14, 'text': 'Рамзан Кадыров', 'type': 'PER'},
 {'index': 68, 'len': 5, 'text': 'Сирию', 'type': 'LOC'},
 {'index': 177, 'len': 17, 'text': 'Минобороны России', 'type': 'ORG'}]
```
---

## Добавление статьи из Wikipedia


```python3
ada.ner_wiki(text,html=True)
```
---
|text|type|wiki_title|wiki_url|wiki_cos|
|--- |--- |--- |--- |--- |
|Чечни|LOC|Чечня|https://ru.wikipedia.org/wiki?curid=1028|0.857749|
|Рамзан Кадыров|PER|Кадыров, Рамзан Ахматович|https://ru.wikipedia.org/wiki?curid=227262|0.971763|
|Сирию|LOC|Сирия|https://ru.wikipedia.org/wiki?curid=9819|0.617805|
|Минобороны России|ORG|Министерство обороны Российской Федерации|https://ru.wikipedia.org/wiki?curid=94444|0.622240|

[Jupyter notebook с демонстрацией](http://nbviewer.jupyter.org/urls/bitbucket.org/popovma/ner/raw/6481e3019f294323580c8378c3c8b0cf3b9b7dfa/Demo.ipynb)

## Параметры для запуска из консоли

-c или --clusters - использовать ли в качестве признака кластеры семантических векторов, по умолчанию True

-m или --model - путь к CRF - модели, по умолчанию ./crf_models/CRF_sg_window_1_size_1000_clusters_500 для /code/NER_script_dialog/NER.py и ./crf_models_window_2/CRF_window_2_sg_window_1_size_1000_clusters_500 для /code/NER_script_dialog/NER_window_2.py

-d или --dictionary - путь к словарю слово-кластер, по умолчанию ./w2v_dicts/sg_window_1_size_1000_clusters_500.pickl

-i или --input - папка, в которой будут находится тексты, из которых необходимо извлечь именованные сущности, по умолчанию ./texts/

-o или --output - папка, в которой будут файлы с разметкой именованных сущностей (FactRuEval 2016), по умолчанию ./NER/

-b или --bio - папка, в которой будут файлы с BIO разметкой, по умолчанию ./BIO/

-p или --parsed - папка с файлами, обработанными Mystem, по умолчанию ./parsed/