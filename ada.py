
# coding: utf-8


from ner.NER import process_input_





import pickle
import numpy as np
import pandas as pd
from pymystem3 import Mystem
m = Mystem()
import string
punct = set(string.punctuation)




import os
import numpy as np
import sys
sys.path.append("./AdaGramPy/src/")
from AdaGram import *
from pymystem3 import Mystem



def prepare_text(adagram_words,text,ners):
    mystem_results = m.analyze(text)
    #print(mystem_results)
    results = []
    for mystem_result in mystem_results:
        #print(mystem_result)
        result = {}
        result['text'] = mystem_result['text']
        if 'analysis' in mystem_result:
            if len(mystem_result['analysis'])>0 and 'lex' in mystem_result['analysis'][0]:
                result['lex'] = mystem_result['analysis'][0]['lex']
            else:
                result['lex'] = mystem_result['text']
            result['word'] = True
        else:
            result['lex'] = mystem_result['text']
            result['word'] = False
        
        if result['lex'] in adagram_words:
            result['in_adagram'] = True
        else:
            result['in_adagram'] = False
        results.append(result)    
            
    for i in range(len(results)):
        if results[i]['word']:
            j=0
            while i+j < len(results) and not any(letter in punct for letter in results[i+j]['lex']) :
                texts = ' '.join([token['lex'].strip() for token in results[i:i+j+1] if token['lex'].strip()!='']).strip()
                if j == 0 or texts != results[i]['texts'][-1][0]:
                    results[i]['texts'] = results[i].get('texts', [])  +[(texts,j)]
                j+=1
    cap = [{'text':'','lex':'','in_wiki':True,'in_adagram':True}]
        
    results = cap*4 + results + cap*4
    #print(results)
    for i  in range(4,len(results)-4):
        if results[i]['in_adagram']:
            #print(results[i])
            left_context = []
            j=1
            while(len(left_context)<4):
                if results[i-j]['in_adagram']:
                    left_context =[results[i-j]['lex']] + left_context
                j+=1
                #print(j,left_context)
            right_context = ['']
            j=1   
            while(len(right_context)<4):
                if results[i+j]['in_adagram']:
                    right_context +=[results[i+j]['lex']]
                j+=1                
            results[i]['context'] = ' '.join(left_context+right_context).strip()
    results = results[4:-5]
    return results




with open('./adagram_words') as f:
    adagram_words = set(f.read().split())




with open('concept_to_article_update_new.pickle', 'rb') as f:
    concept_to_article = pickle.load(f)




with open('wiki_dict_part_new_2_add.pickle', 'rb') as f:
    wiki_dict = pickle.load(f)




concepts = sorted(concept_to_article.keys(),key = lambda x: len(x.split()),reverse=True)



vm, vocab = load_model("./AdaGramPy/model/model_wiki") 




def create_vectors(text):
    i=0
    while i <len(text):
        if 'articles' in text[i] :
            vec = np.zeros(300,dtype=np.float32)
            j = text[i]['concept'][1]
            for m in range(j+1):
                if text[i+m]['in_adagram']:
                    sences_prob = disambiguate(vm, vocab, text[i+m]['lex'], text[i+m]['context'].split())
                    sence = np.argmax(sences_prob)
                    word_vec = vm.In[:, sence, vocab.word2id[text[i+m]['lex']]]
                    word_vec /= np.linalg.norm(word_vec)
                    vec += word_vec
            text[i]['vec'] = vec
            text[i]['vec2']=vec
            i += j
        elif text[i]['in_adagram']:
            sences_prob = disambiguate(vm, vocab, text[i]['lex'], text[i]['context'].split())
            sence = np.argmax(sences_prob)
            word_vec = vm.In[:, sence, vocab.word2id[text[i]['lex']]]
            word_vec /= np.linalg.norm(word_vec)
            text[i]['vec2']=word_vec
                    
        i+=1




def find_wiki_concepts(text):
    i=0
    while i <len(text):
        if 'texts' in text[i]:
            candidates = sorted(text[i]['texts'],key = lambda x: len(x[0].split()),reverse=True)
            for candidate in candidates:
                if candidate[0] in concepts:
                    text[i]['concept'] = candidate
                    text[i]['articles'] = concept_to_article[candidate[0]]
                    i += candidate[1]
                    break
                    
        i+=1    
                        




def find_wiki(text):
    for token in text:
        if 'vec' in token and  not np.array_equal(token['vec'], np.zeros(300,dtype=np.float32)):
            max_cos = -2
            max_article = {}
            for article in token['articles']:
                try:
                    #print('try')
                    wiki_articles = wiki_dict[article]
                    
                    for wiki_article in wiki_articles:
                        #print(wiki_article)
                        articles_cos = np.dot(wiki_article['vec'],token['vec'])/np.linalg.norm(wiki_article['vec'])/np.linalg.norm(token['vec'])
                        
                        #print('='*100)
                        
                        if max_cos < articles_cos:
                            
                            max_cos = articles_cos
                    
                            token['wiki_title']=wiki_article['title']
                            
                            token['wiki_url']=wiki_article['url']
                            token['wiki_id']=wiki_article['id']
                            token['wiki_cos']= max_cos
                except:
                    pass




def result_function(text,ners):
    text = prepare_text(adagram_words,text,ners )
    
    for item in text:
        if 'texts' in item:
            item['texts'] = [i for i in item['texts'] if i[0] in ners]
    find_wiki_concepts(text)
    create_vectors(text)
    find_wiki(text)
    #print(text)
    results = []
    index = 0
    for i,token in enumerate(text):
        result = {}
        result['lex'] = token['lex']
        result['text'] = token['text']
        result['index'] = index
        index += len(token['text'])
        if 'vec2' in token and not np.array_equal(np.zeros(300,dtype=np.float32),token['vec2']):
            result['vec2']=token['vec2']
        if 'wiki_cos' in token:
            result['wiki_cos'] = token['wiki_cos']
            result['wiki_url'] = token['wiki_url'].strip()
            result['wiki_title'] = token['wiki_title'].strip()
            result['concept'] = token['concept'][0]
            result['len_concept'] = token['concept'][1]
            
        results.append(result)
    df = pd.DataFrame(results)
    #df = df[['text','lex','concept','len_concept','wiki_cos','wiki_title','wiki_url']]
    #df.to_csv(out_file)
    return df



def ner_wiki(text, html = False):
    ner_results = process_input_(text)
    ners = [item['text'] for item in ner_results]
    ners = [''.join(m.lemmatize(item)).strip() for item in ners]
    df = result_function(text,ners)
    
    
    for res in ner_results:
        if list(df[df['index'] == res['index']]['len_concept'].notnull())[0]:
            res['concept'] = list(df[df['index'] == res['index']]['concept'])[0]
            res['wiki_title'] = list(df[df['index'] == res['index']]['wiki_title'])[0]
            res['wiki_url'] = list(df[df['index'] == res['index']]['wiki_url'])[0]
            res['wiki_cos'] = list(df[df['index'] == res['index']]['wiki_cos'])[0]
    df = pd.DataFrame(ner_results)
    if html:
     return df[['text','type','wiki_title','wiki_url','wiki_cos']].to_html().replace('\n','')
    else:
     return df[['text','type','wiki_title','wiki_url','wiki_cos']]
            
            






